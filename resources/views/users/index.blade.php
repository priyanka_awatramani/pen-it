@extends('layouts.app')
@section('content')

    <div class="d-flex justify-content-end mb-3 mt-3">
        
    </div>

    <div class="card">
        <div class="card-header">
            Users
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Post Count</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>
                            <img src="{{\Thomaswelton\LaravelGravatar\Facades\Gravatar::src($user->email)}}" alt="">
                        </td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if(!$user->isAdmin())
                            <form action="{{ route('users.make-admin', $user->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <button type="submit" class="btn btn-outline-danger">Make Admin</button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
    <!---Delete Modal--->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                </div>
            <div class="modal-body">
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p>Are you sure you wanna delete</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-danger">
                        Delete Category
                    </button>
                </div>
            </form>
            </div>
            </div>
    </div>
@endsection

@section('page-level-scripts')
    <script>
        function displayModalForm($category){
            var url = '/categories/'+$category.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection